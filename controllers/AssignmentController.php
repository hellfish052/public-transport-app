<?php

require 'models/Assignment.php';


class AssignmentContoler
{
    private $assignment;

    public function __construct()
    {
        $this->assignment = new Assignment();
    }

    public function save()
    {
        if (isset($_POST)) {
            if ($this->bus->newAssignment($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            if ($this->bus->updateAssignment($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }
}
