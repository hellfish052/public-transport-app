<main class="container py-5">

    <div class="row">
        <div class="col-12 text-center mb-5">
            <h1>Conductor</h1>
        </div>
    </div>

    <section class="row d-flex justify-content-center">
        <div class="col-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <b>Nuevo Conductor</b>
                </div>

                <div class="card-body">
                    <form id="formDriver">
                        <div class="form-group">
                            <label for="firstname">Identificación</label>
                            <input id="identification" type="text" class="form-control" placeholder="Ingrese Identificacion">
                        </div>
                        <div class="form-group">
                            <label for="firstName">Nombres</label>
                            <input id="firstName" type="text" class="form-control" placeholder="Ingrese Nombres">
                        </div>
                        <div class="form-group">
                            <label for="lastName">Apellidos</label>
                            <input id="lastName" type="text" class="form-control" placeholder="Ingrese Apellidos">
                        </div>
                        <div class="form-group">
                            <button id="btnSaveDriver" type="submit" class="btn btn-success">Agregar <i class="fas fa-plus-square fa-lg"></i></button>
                            <a href="?controller=driver" class="btn btn-secondary">Cancelar <i class="fas fa-window-close"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>