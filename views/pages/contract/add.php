<main class="container py-5">

    <div class="row">
        <div class="col-12 text-center mb-5">
            <h1>Contrato</h1>
        </div>
    </div>

    <section class="row d-flex justify-content-center">
        <div class="col-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <b>Nuevo Contracto</b>
                </div>

                <div class="card-body">
                    <form id="formContract">
                        <div class="form-group">
                            <label for="start_date">fecha de inicio</label>
                            <input id="start_date" type="date" class="form-control" placeholder="Ingrese Identificacion">
                        </div>
                        <div class="form-group">
                            <label for="final_date">fecha de finalizacion</label>
                            <input id="final_date" type="date" class="form-control" placeholder="Ingrese Nombres">
                        </div>
                        <div class="form-group">
                            <label>Conductor</label>
                            <select id="driver" class="form-control">
                                <option value="">Seleccione...</option>
                                <?php foreach ($drivers as $driver) : ?>
                                    <option value="<?php echo $driver->id; ?>">
                                        <?php echo $driver->first_name . ' ' . $driver->last_name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="total_value">Valor del contrato</label>
                            <input id="total_value" type="number" min="0" class="form-control" placeholder="Ingrese valor final">
                        </div>
                        <div class=" form-group">
                            <button id="btnSaveContract" type="submit" class="btn btn-success">Agregar <i class="fas fa-plus-square fa-lg"></i></button>
                            <a href="?controller=driver" class="btn btn-secondary">Cancelar <i class="fas fa-window-close"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>