function addDriver(){
    let identification = $('#identification').val();
    let firstName = $('#firstName').val();
    let lastName = $('#lastName').val();

    
    if(!identification =='' && !firstName == '' && !lastName == ''){
        let Data = {
            'identification': identification,
            'first_name': firstName,
            'last_name': lastName,
        }
        $.ajax({
            url:"?controller=driver&method=save",//send data to add.php to add the event to database
            type:"POST",
            data:Data,
            success:function(response)
            {
                let {success} = JSON.parse(response);
                console.log(success);
                if(success){
                    alert("Conductor registrado");
                    window.location = '?controller=driver';
                }
                else{
                    alert("Error agregando conductor");
                }
            }
        });

    }
    else{
        alert('¡Uno o mas datos vacíos! intente nuevamente')
    }
}

function editDriver(){
    let id = $('#id').val();
    let identification = $('#identification').val();
    let firstName = $('#firstName').val();
    let lastName = $('#lastName').val();


    if(!identification =='' && !firstName == '' && !lastName == ''){
        
        let Data = {
            'id': id,
            'identification': identification,
            'first_name': firstName,
            'last_name': lastName,
        }


        $.ajax({
        url:"?controller=driver&method=update",//send data to add.php to add the event to database
        type:"POST",
        data:Data,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("¡Conductor Editado!");
                window.location = '?controller=driver';
            }
            else{
                alert("Error actualizando el conductor");
            }
        }
        });
    }
    else{
        alert('¡Uno o mas datos vacíos! intente nuevamente')
    }

}


$(document).ready(function() {  	
	$('#formDriver').submit(function(e) {
    e.preventDefault();
    });
    $('#btnSaveDriver').click(function(e) {
    e.preventDefault();
		addDriver();
    });
	$('#btnUpdateDriver').click(function(e) {
    e.preventDefault();
		editDriver();
    });
});